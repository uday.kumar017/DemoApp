Rails.application.routes.draw do
  resources :comments
  resources :tasks
  resources :projects
  resources :students
  resources :teachers
  resources :appoitments
  resources :patients
  resources :doctors
  #get 'movies/index'
  resources :movies
  resources :books
  resources :authors
  resources :employees
  root "authors#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
