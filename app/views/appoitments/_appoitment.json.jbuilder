json.extract! appoitment, :id, :doctor_id, :patient_id, :created_at, :updated_at
json.url appoitment_url(appoitment, format: :json)
