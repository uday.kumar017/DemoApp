class MoviesController < ApplicationController
  def index
  	@movies = Movie.all
  end

  def new
  	@movie = Movie.new
  end

  def create  	
  	@movie = Movie.new(name: params[:movie][:name],director_name: params[:movie][:director_name])
  	if @movie.save
  		redirect_to movies_path
  	else
  		redirect_to new_movie_path
  	end
  end

  
  def edit
  	@movie = Movie.find_by(id: params[:id])
  end

  def update
  	@movie = Movie.find_by(id: params[:id])
  	if @movie.update(name: params[:movie][:name],director_name: params[:movie][:director_name])
  		redirect_to movies_path
  	else
  		redirect_to movie_path(@movie)
  	end
  end


def destroy
	@movie = Movie.find_by(id: params[:id])
	if @movie.present?
		@movie.destroy
		redirect_to movies_path
	end
end

end
